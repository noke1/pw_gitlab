import { test as base } from '@playwright/test';

import { ProjectDashboard } from './../pages/project/ProjectDashboard';
import { LeftSideMenu } from './../pages/LeftSideMenu';
import { Dashboard } from '../pages/Dashboard';
import { Login } from '../pages/Login';
import { NewIssue } from '../pages/issue/NewIssue';
import { IssueDetails } from '../pages/issue/IssueDetails';
import { RightSideIssueDetailsMenu } from '../pages/issue/RightSideIssueDetailsMenu';
import { NewUser } from '../pages/admin/users/NewUser';
import { UserDetails } from '../pages/admin/users/UserDetails';
import { UsersDashboard } from '../pages/admin/users/UsersDashboard';
import { AdminProjectsDashboard } from './../pages/admin/projects/AdminProjectsDashboard';
import { ProjectMembers } from '../pages/project/ProjectMembers';

export type PagesFixture = {
  dashboard: Dashboard
  leftSideMenu: LeftSideMenu
  login: Login
  projectDashboard: ProjectDashboard
  newIssue: NewIssue
  issueDetails: IssueDetails
  rightSideIssueDetailsMenu: RightSideIssueDetailsMenu
  newUser: NewUser
  userDetails: UserDetails
  usersDashboard: UsersDashboard
  adminProjectsDashboard: AdminProjectsDashboard
  projectMembers: ProjectMembers
};

export const test = base.extend<PagesFixture>({
  dashboard: async ({ page }, use) => {
    await use(new Dashboard(page));
  },
  leftSideMenu: async ({ page }, use) => {
    await use(new LeftSideMenu(page));
  },
  login: async ({ page }, use) => {
    await use(new Login(page));
  },
  projectDashboard: async ({ page }, use) => {
    await use(new ProjectDashboard(page));
  },
  newIssue: async ({ page }, use) => {
    await use(new NewIssue(page));
  },
  issueDetails: async ({ page }, use) => {
    await use(new IssueDetails(page));
  },
  rightSideIssueDetailsMenu: async ({ page }, use) => {
    await use(new RightSideIssueDetailsMenu(page));
  },
  newUser: async ({ page }, use) => {
    await use(new NewUser(page));
  },
  userDetails: async ({ page }, use) => {
    await use(new UserDetails(page));
  },
  usersDashboard: async ({ page }, use) => {
    await use(new UsersDashboard(page));
  },
  adminProjectsDashboard: async ({ page }, use) => {
    await use(new AdminProjectsDashboard(page));
  },
  projectMembers: async ({ page }, use) => {
    await use(new ProjectMembers(page));
  },
});
