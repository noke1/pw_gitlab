/* eslint-disable no-empty-pattern */
import { test as base } from '@playwright/test';

import { UserResource } from '../api/UserResource';
import { ProjectResource } from '../api/ProjectResource';
import { CallContext } from '../api/CallContext';

import { PersonalAccessTokenRequest } from '../models/PersonalAccessToken';
import { ProjectRequest } from '../models/Project';
import { IssueRequest } from '../models/Issue';
import { MemberRequest } from '../models/Member';
import { User } from '../models/User';

export type ApiFixture = {
  setupUser: (userData: User) => Promise<number>
  setupAccessToken: (tokenData: PersonalAccessTokenRequest) => Promise<string>
  setupProject: (
    projectData: ProjectRequest,
    accessToken?: string
  ) => Promise<number>
  setupIssue: (
    issueData: IssueRequest,
    projectId: number,
    accessToken?: string
  ) => Promise<number>
  setupUserInProject: (
    projectId: number,
    member: MemberRequest
  ) => Promise<void>
  banUser: (userId: number) => Promise<void>
  deactivateUser: (userId: number) => Promise<void>
  deleteUser: (userId: number) => Promise<void>
};

export const test = base.extend<ApiFixture>({
  setupUser: async ({}, use) => {
    await use(async (userData) => {
      const asAdmin: CallContext = new CallContext();
      const userResource: UserResource = new UserResource(asAdmin);

      const { id: userId } = await userResource.createUser(userData);
      return userId;
    });
  },
  setupAccessToken: async ({}, use) => {
    await use(async (tokenData) => {
      const asAdmin: CallContext = new CallContext();
      const userResource: UserResource = new UserResource(asAdmin);

      const { token: accessToken } =
        await userResource.generatePersonalAccessToken(tokenData);
      return accessToken;
    });
  },
  setupProject: async ({}, use) => {
    await use(async (projectData, accessToken?) => {
      let context: CallContext;
      if (accessToken) {
        context = new CallContext(accessToken);
      } else {
        context = new CallContext();
      }
      const projectResource: ProjectResource = new ProjectResource(context);

      const { id: projectId } = await projectResource.createProject(projectData);
      return projectId;
    });
  },
  setupIssue: async ({}, use) => {
    await use(async (issueData, projectId, accessToken) => {
      let context: CallContext;
      if (accessToken) {
        context = new CallContext(accessToken);
      } else {
        context = new CallContext();
      }
      const projectResource: ProjectResource = new ProjectResource(context);
      const { iid: issueId } = await projectResource.createIssue(
        projectId,
        issueData,
      );

      return issueId;
    });
  },
  setupUserInProject: async ({}, use) => {
    await use(async (projectId, member) => {
      const asAdmin: CallContext = new CallContext();
      const projectResource: ProjectResource = new ProjectResource(asAdmin);
      await projectResource.addMemberToProject(projectId, member);
    });
  },
  banUser: async ({}, use) => {
    await use(async (userId) => {
      const asAdmin: CallContext = new CallContext();
      const userResource: UserResource = new UserResource(asAdmin);

      await userResource.banUser(userId);
    });
  },
  deleteUser: async ({}, use) => {
    await use(async (userId) => {
      const asAdmin: CallContext = new CallContext();
      const userResource: UserResource = new UserResource(asAdmin);

      await userResource.deleteUser(userId);
    });
  },
  deactivateUser: async ({}, use) => {
    await use(async (userId) => {
      const asAdmin: CallContext = new CallContext();
      const userResource: UserResource = new UserResource(asAdmin);

      await userResource.deactivateUser(userId);
    });
  },
});
