import { test as base } from '@playwright/test';

import { loadTranslationFor } from '../utils/translations';

import { Translation } from '../models/Translation';

export type UtilsFixture = {
  getTranslations: () => Translation
};

export const test = base.extend<UtilsFixture>({
  getTranslations: async ({ locale }, use) => {
    await use(() => {
      const jsonTranslations = loadTranslationFor(locale!);
      return jsonTranslations;
    });
  },
});
