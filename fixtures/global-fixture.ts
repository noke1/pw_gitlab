import { mergeTests } from '@playwright/test';
import { test as apiFixture } from './api-fixture';
import { test as pagesFixture } from './pages-fixture';
import { test as utilsFixture } from './utils-fixture';

export const test = mergeTests(apiFixture, pagesFixture, utilsFixture);
