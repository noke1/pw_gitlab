import { faker } from '@faker-js/faker';

export class ProjectProvider {
  generateRandomPublicProject(): ProjectRequest {
    return {
      name: `API-${faker.word.verb()}-${faker.number.int()}`,
      visibility: 'public',
    };
  }
}
