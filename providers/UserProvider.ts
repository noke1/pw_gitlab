import { faker } from '@faker-js/faker';

export class UserProvider {
  generateRandomRegularUser(): User {
    return {
      email: faker.internet.email(),
      username: faker.internet.userName(),
      name: faker.person.fullName(),
      password: faker.internet.password({ length: 9, pattern: /[A-Z]/ }),
    };
  }

  generateRandomAdmin(): User {
    return {
      ...this.generateRandomRegularUser(),
      admin: true,
    };
  }
}
