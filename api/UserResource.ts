import {
  type APIRequestContext,
  type APIResponse,
  request,
  expect,
} from '@playwright/test';

import { CallContext } from './CallContext';

import dotenv from 'dotenv';

import { User, UserResponse } from '../models/User';
import { PersonalAccessTokenRequest, PersonalAccessTokenResponse } from '../models/PersonalAccessToken';

dotenv.config();

export class UserResource {
  readonly callContext: CallContext;

  constructor(callContext: CallContext) {
    this.callContext = callContext;
  }

  /**
   * Method to create the user inside the gitlab.
   * This resource is accessible only for admin users.
   * @param userData user to be created
   * @returns user data as an api response
   */
  async createUser(userData: User): Promise<UserResponse> {
    const context: APIRequestContext = await request.newContext();

    const userResponse: APIResponse = await context.post('/api/v4/users', {
      headers: {
        'private-token': this.callContext.accessToken,
      },
      data: userData,
      ignoreHTTPSErrors: true,
    });
    expect(userResponse.status()).toBe(201);

    const createdUser: UserResponse = await userResponse.json();
    return createdUser;
  }

  /**
   * Resource to ban a user with the specific userId
   * @param userId Id of the user to be banned
   */
  async banUser(userId: number): Promise<void> {
    const context: APIRequestContext = await request.newContext({});

    const userResponse: APIResponse = await context.post(
      `api/v4/users/${userId}/ban`,
      {
        headers: {
          'private-token': this.callContext.accessToken,
        },
      },
    );
    expect(userResponse.status()).toBe(201);
  }

  /**
   * Method to deactivate a user with the specific userId
   * @param userId user id to be deactivated
   */
  async deactivateUser(userId: number): Promise<void> {
    const context: APIRequestContext = await request.newContext({});

    const userResponse: APIResponse = await context.post(
      `api/v4/users/${userId}/deactivate`,
      {
        headers: {
          'private-token': this.callContext.accessToken,
        },
      },
    );
    expect(userResponse.status()).toBe(201);
  }

  async searchUserByParams(
    searchParams: Record<string, string>,
  ): Promise<UserResponse[]> {
    const searchQueryParams: string[] = [];
    for (const key in searchParams) {
      const value: string = searchParams[key];
      searchQueryParams.push(`${key}=${value}`);
    }

    const context: APIRequestContext = await request.newContext({});
    const userResponse: APIResponse = await context.get(
      `api/v4/users?${searchQueryParams.join('&')}`,
      {
        headers: {
          'private-token': this.callContext.accessToken,
        },
      },
    );
    expect(userResponse.status()).toBe(200);

    const searchedUsers: UserResponse[] = await userResponse.json();
    return searchedUsers;
  }

  /**
   *  Function to create an access token for a user. This api resource is accessible only for admin users.
   * @param personalAccessToken
   * @returns
   */
  async generatePersonalAccessToken(
    personalAccessToken: PersonalAccessTokenRequest,
  ): Promise<PersonalAccessTokenResponse> {
    const context: APIRequestContext = await request.newContext({});

    const accessTokenResponse: APIResponse = await context.post(
      `api/v4/users/${personalAccessToken.user_id}/personal_access_tokens`,
      {
        headers: {
          'private-token': `${process.env.ADMIN_ACCESS_TOKEN}`,
        },
        data: personalAccessToken,
      },
    );
    expect(accessTokenResponse.status()).toEqual(201);

    return accessTokenResponse.json();
  }

  /**
   * Resource to ban a user with the specific userId
   * @param userId Id of the user to be banned
   */
  async deleteUser(userId: number): Promise<void> {
    const context: APIRequestContext = await request.newContext({});

    const userResponse: APIResponse = await context.delete(
      `api/v4/users/${userId}`,
      {
        headers: {
          'private-token': this.callContext.accessToken,
        },
      },
    );
    expect(userResponse.status()).toBe(204);
  }
}
