import { type APIRequestContext, type APIResponse } from '@playwright/test';
import { CallContext } from './CallContext';

export class RequestHandler {
  async apiPostRequest(
    context: Promise<APIRequestContext>,
    callContext: CallContext,
    path: string,
    data: any,
  ): Promise<APIResponse> {
    const caller = await context;
    const response = await caller.post(path, {
      headers: {
        'private-token': callContext.accessToken,
      },
      data: data,
    });
    // Add error handling here if needed
    return response;
  }
}
