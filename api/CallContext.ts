import dotenv from 'dotenv';
dotenv.config();

export class CallContext {
  readonly accessToken: string;

  constructor(accessToken = process.env.ADMIN_ACCESS_TOKEN as string) {
    this.accessToken = accessToken;
  }
}
