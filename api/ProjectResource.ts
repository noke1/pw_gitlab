import {
  type APIRequestContext,
  type APIResponse,
  request,
  expect,
} from '@playwright/test';
import { CallContext } from './CallContext';
import { RequestHandler } from './RequestHandler';

import { ProjectRequest, ProjectResponse } from '../models/Project';
import { MemberRequest } from '../models/Member';
import { IssueRequest, IssueResponse } from '../models/Issue';

export class ProjectResource {
  private readonly callContext: CallContext;

  private readonly apiContext: Promise<APIRequestContext>;

  private readonly requestHandler: RequestHandler;

  constructor(callContext: CallContext) {
    this.callContext = callContext;
    this.apiContext = request.newContext({});
    this.requestHandler = new RequestHandler();
  }

  async createProject(project: ProjectRequest): Promise<ProjectResponse> {
    const projectResponse: APIResponse =
      await this.requestHandler.apiPostRequest(
        this.apiContext,
        this.callContext,
        'api/v4/projects',
        project,
      );

    expect(projectResponse.status()).toEqual(201);

    return projectResponse.json();
  }

  async addMemberToProject(
    projectId: number,
    member: MemberRequest,
  ): Promise<void> {
    await this.requestHandler.apiPostRequest(
      this.apiContext,
      this.callContext,
      `api/v4/projects/${projectId}/members`,
      member,
    );
  }

  async createIssue(
    projectId: number,
    issue: IssueRequest,
  ): Promise<IssueResponse> {
    const issueResponse: APIResponse = await this.requestHandler.apiPostRequest(
      this.apiContext,
      this.callContext,
      `api/v4/projects/${projectId}/issues`,
      issue,
    );

    expect(issueResponse.status()).toEqual(201);

    return issueResponse.json();
  }
}
