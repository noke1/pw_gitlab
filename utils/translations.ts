import * as fs from 'fs';

export const loadTranslationFor = (localeName: string): Translation => {
  const t = fs.readFileSync(`locale/${localeName}.json`, 'utf-8');
  return JSON.parse(t);
};
