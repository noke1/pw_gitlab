/**
 * Function to get rid of any additional spaces, tabs, new lines, non-breaking spaces
 * @param text string to be normalized
 * @returns normalized string
 */
export const normalizeText = (text: string) => {
  if (!text) throw new Error('Empty string was passed!');

  return (
    text
      .replace(/&nbsp;/gi, '')
      //Regexp to replace all more than 1 spaces, tabs, newlines
      .replace(/\s\s+/g, ' ')
      .trim()
  );
};
