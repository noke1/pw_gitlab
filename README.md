# Playwright GitLab tests

## Summary

Another test automation practice project. This time I used the GitLab system to perform the testing.

## Considerations

1. I did not care about the scenarios that were automated. The test cases we to be automated were not prepared before the coding. I just thought of any crazy scenario and tried to automate it as best as I can. It went pretty well.
2. With the above being said I also do not care much about the test categorization (like @smoke, @regression etc). The tags for the tests were assigned kind of randomly just to check how to handle tagging in playwright.
3. I was not able to practice any CI/CD within this project. All of my free trial accounts on cloud providers are expired already and I don't want to spent much money on this :)
4. There is still a lot to improve here:
   - move the non-test methods from test files into some other files/classes maybe
   - try to create and run the tests for any mobile viewport
   - I don't like the way I am creating a new context per each resource call - could be improved
   - maybe there is a better way to somehow abstract the api methods as a generic?
5. What I wanted to try out:
   - [x] learn playwright for UI testing
   - [x] learn playwright for API usage
   - [x] compare the playwright with Cypress
   - [ ] use some non-standard playwright libraries like: how to query a sql db, how to do some file comparisons etc. Something that is not handled by playwright itself
   - [x] try to use some configurational tests -> Some of the tests have the transalations in the .json files which is fetched into the test based on the locale setting. I reused the locale from playwright config fiel
   - [x] check if the playwright is as fun to use as people say - I have some considerations about it
   - [x] learn much more Typescript stuff
   - [x] how hard it is to use the GitLab locally and setup the tests for it. And I also faced some real project issues even here xD (ehh reminds me of the real job problems I faced)
   - [ ] practice testing in CI/CD. It is done partially becouse I amanged to establish the tests to be run in a pipeline. However there are some problems:
     - need to deploy the gitlab app during the pipeline - this was solved
     - wait for the GitLab app to start - this was solved
     - run the tests - this was solved - this was solved
     - crete an allure-report -> this was not solved becouse I am running a free gitlab version and the pipelines time is restriced to like 400 minutes a month. So after a few attemps I reached the limit and didnt want to wait another month :)
     - and this step with generating the allure-results is quite a dummy step becouse in the real world I will try to move the allure-results file into the server when I can spin up the reports. Right now it onyl saves the folder so it can be downloaded to local and executed to create a visual report. The same goes for playwright-report folder

## Setup

1. To run the tests a local instance of GitLab is needed. I setup the local instance via a docker image
2. Download the test repository into the local pc
3. Go to the root directory and create a `.env` file with: (or add the values into the `.env-sample` file and save it as `.env`)
   - `BASE_URL` - with the gitlab instance localhost address
   - `ADMIN_ACCESS_TOKEN` - with the personal access token for the admin. The admin role and access token is needed becouse I am using the API to create a lot of data inside the tests. Personal access token needs to be created manually inside the GitLab admin account. Or you can run the following script INSIDE the gitlab docker container that will create a token `gitlab-rails runner "token = User.find_by_username('root').personal_access_tokens.create(scopes: ['api'], name: 'PW_GITLAB_AUTO', expires_at: 365.days.from_now); token.set_token('glpat-eCMSd8h9WgDBHDPcSn13'); token.save!"`
4. To run the tests pick a script from a `package.json` file
