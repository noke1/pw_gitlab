import { expect as baseExpect } from '@playwright/test';

type SortDirection = 'asc' | 'desc';
type Options = {
  direction: SortDirection
};

export const expect = baseExpect.extend({
  /**
   * Custom matcher to check if the given STRING array is sorted ascending or descending
   * @param received string array
   * @param options options to pick from SortDirection type - 'asc' or 'desc'
   * @returns a matcher outcome success if received array is sorted in a requested direction
   */
  toBeSorted(received: string[], options: Options) {
    const copied = [...received];

    if (options.direction === 'asc') {
      copied.sort((a, b) => a.localeCompare(b));
    } else if (options.direction === 'desc') {
      copied.sort((a, b) => b.localeCompare(a));
    }

    const pass: boolean = received.every((el, idx) => el === copied[idx]);

    const message = pass
      ? () => 'passed'
      : () =>
        `toBeSorted() custom matcher failed! Expected the given array [${received}] to be sorted ${options.direction} but the sorted array is [${copied}]`;
    return {
      message,
      pass,
      name: 'toBeSorted',
    };
  },
});
