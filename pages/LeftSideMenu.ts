import { type Locator, type Page } from '@playwright/test';

export class LeftSideMenu {
  readonly page: Page;

  readonly avatar: Locator;

  readonly signOutAnchor: Locator;

  readonly issuesAnchor: Locator;

  readonly mainMenuOptionsUl: Locator;

  constructor(page: Page) {
    this.page = page;
    this.avatar = page.getByTestId('user-avatar-content');
    this.signOutAnchor = page.getByTestId('sign-out-link');
    this.issuesAnchor = page.locator('#pinned [data-qa-submenu-item="Issues"]');
    this.mainMenuOptionsUl = page.locator(
      '[data-testid="non-static-items-section"]',
    );
  }

  async expandAvatar() {
    await this.avatar.click();
  }

  async signOut() {
    await this.expandAvatar();
    await this.signOutAnchor.click();
  }

  async goToIssues() {
    await this.issuesAnchor.click();
  }

  async goToOption(option: MainOptionNames) {
    const mainOption = this.mainMenuOptionsUl.locator(
      `button[data-qa-section-name="${option}"]`,
    );
    await mainOption.click();
    return async (subOption: SubOptionNames) => {
      await this.mainMenuOptionsUl
        .locator(`[data-qa-submenu-item="${subOption}"]`)
        .click();
    };
  }
}

type MainOptionNames =
  | 'Manage'
  | 'Plan'
  | 'Code'
  | 'Build'
  | 'Secure'
  | 'Deploy'
  | 'Operate'
  | 'Monitor'
  | 'Analyze'
  | 'Settings';

type SubOptionNames = 'Activity' | 'Members' | 'Labels';
