import { type Locator, type Page } from '@playwright/test';

export class ProjectDashboard {
  readonly page: Page;

  readonly welcomeMessage: Locator;

  readonly newIssueButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.welcomeMessage = page.locator(
      '[data-qa-selector="welcome_title_content"]',
    );
    this.newIssueButton = page
      .getByTestId('gl-empty-state-content')
      .locator('span', { hasText: 'New issue' });
  }

  async visit(username: string, projectname: string) {
    await this.page.goto(`/${username}/${projectname}`);
  }

  async clickNewIssue() {
    await this.newIssueButton.click();
  }
}
