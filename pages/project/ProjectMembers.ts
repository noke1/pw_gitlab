import { type Locator, type Page, expect } from '@playwright/test';
import { normalizeText } from '../../utils/texts';

export class ProjectMembers {
  readonly page: Page;

  readonly inviteMembersButton: Locator;

  readonly usernameOrEmailSearchInput: Locator;

  readonly accessExpirationInput: Locator;

  readonly confirmInviteButton: Locator;

  //This can be a list of suggestions as well as only one. Depends on the results found
  readonly inviteSuggestionsButtons: Locator;

  readonly rolesSelect: Locator;

  readonly membersTable: Locator;

  readonly alertDiv: Locator;

  constructor(page: Page) {
    this.page = page;
    this.inviteMembersButton = this.page.locator('button[data-testid="invite-members-button"]');
    this.usernameOrEmailSearchInput = this.page.getByTestId('members-token-select-input');
    this.accessExpirationInput = this.page.getByPlaceholder('YYYY-MM-DD');
    this.confirmInviteButton = this.page.locator('button[data-testid="invite-modal-submit"]');
    this.inviteSuggestionsButtons = this.page.locator('button[id^="token-selector"]');
    this.rolesSelect = this.page.locator('select[data-qa-selector="access_level_dropdown"]');
    this.membersTable = this.page.locator('[data-testid^="members-table-row-"]');
    this.alertDiv = this.page.getByRole('status').locator('div.gl-alert-body');
  }

  async visit(username: string, projectname: string) {
    await this.page.goto(`/${username}/${projectname}/-/project_members`);
  }

  async inviteMember(memberData: ProjectMemberRow) {
    await this.inviteMembersButton.click();
    await this.fillInviteForm({
      username: memberData.username,
      role: memberData.role,
      expirationDate: memberData.expirationDate,
    } satisfies ProjectMemberRow);
    await this.confirmInviteButton.click();
  }

  async getProjectMembers() {
    await expect(async () => {
      const rowsFound = await this.membersTable.count();
      expect(rowsFound, 'There should be a new entry added to the members row!').toBeGreaterThan(1);
    }).toPass({
      timeout: 10_000,
      intervals: [1_000, 4_000, 9_000],
    });

    const projectMembers: ProjectMemberRow[] = [];
    for (let i = 0; i < await this.membersTable.count(); i++) {
      const row = this.membersTable.nth(i);
      const username = await row.locator('[aria-colindex="1"] span.gl-avatar-labeled-sublabel').textContent() || '';
      //Role can ba a dropdown or just a div... To overcome this or() was used
      const role = await row.locator('[aria-colindex="3"] span.gl-new-dropdown-button-text')
        .or(row.locator('[aria-colindex="3"] div[data-testid="role-text"]'))
        .textContent() || '';
      const expirationDate = await row.locator('[aria-colindex="4"] input').inputValue() || '';
      const member: ProjectMemberRow = {
        username: normalizeText(username),
        role: normalizeText(role),
        expirationDate,
      };
      projectMembers.push(member);
    }
    return projectMembers;
  }

  private async fillInviteForm({ username, role, expirationDate }: ProjectMemberRow) {
    await this.usernameOrEmailSearchInput.fill(username);
    await expect(this.inviteSuggestionsButtons).toBeVisible();
    await this.usernameOrEmailSearchInput.press('Enter');

    await this.page.getByRole('button', { name: 'Guest' }).click();
    const roles = this.page.locator('[data-testid^="listbox-item-role-static"] div');
    for (let i = 0; i < await roles.count(); i++) {
      const currentRole = roles.nth(i);
      const roleName = await currentRole.textContent();
      if (roleName === role) {
        await currentRole.click();
        break;
      }
    }

    if (expirationDate) {
      await this.accessExpirationInput.fill(expirationDate);
    }
  }
}

export type ProjectMemberRow = {
  username: string,
  role: string,
  expirationDate?: string
};


