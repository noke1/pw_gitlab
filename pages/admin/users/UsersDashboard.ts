import { type Locator, type Page } from '@playwright/test';

import { normalizeText } from '../../../utils/texts';

import { UserDashboardData } from '../../../models/User';

export class UsersDashboard {
  readonly page: Page;

  readonly newUserButton: Locator;

  readonly flashAlerts: Locator;

  readonly nextPageButton: Locator;

  readonly usersTableRows: Locator;

  readonly searchInput: Locator;

  readonly currentSortingButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.newUserButton = this.page.locator('span', { hasText: 'New user' });
    this.flashAlerts = this.page.locator('[data-testid="flash-container"]');
    this.nextPageButton = this.page.locator('li.js-next-button');
    this.usersTableRows = this.page.locator('[data-testid="user-row-content"]');
    this.searchInput = this.page.locator('#search_query');
    this.currentSortingButton = this.page.locator(
      '#dropdown-toggle-btn-1 .gl-new-dropdown-button-text',
    );
  }

  async visit() {
    const getUsers = this.page.waitForResponse('**/admin/users');
    const getUsersGraphql = this.page.waitForResponse('**/api/graphql');
    await this.page.goto('/admin/users');
    await getUsers;
    await getUsersGraphql;
  }

  async goToNewUser() {
    await this.newUserButton.click();
  }

  async getAlertsTexts() {
    return (await this.flashAlerts.allTextContents()).map(normalizeText);
  }

  async goToNextPage() {
    const nextPageUsersResponse = this.page.waitForResponse(
      '**/admin/users?page**',
    );
    const nextPageUsersGraphqlResponse =
      this.page.waitForResponse('**/api/graphql');

    await this.nextPageButton.click();

    await nextPageUsersGraphqlResponse;
    await nextPageUsersResponse;
  }

  async isButtonNextPageDisabled() {
    return (await this.nextPageButton.getAttribute('class'))
      ?.split(' ')
      .includes('disabled');
  }

  async searchForUser(searchPhrase: string) {
    const userSearchResponse = this.page.waitForResponse(
      '/admin/users?search_query=**',
    );
    await this.searchInput.fill(searchPhrase);
    await this.page.keyboard.press('Enter');
    await userSearchResponse;
    await this.page.waitForURL('/admin/users?search_query=**');
  }

  async getTableRowsContent() {
    const usersFound: UserDashboardData[] = [];
    const usersFoundCount = await this.usersTableRows.count();
    for (let i = 0; i < usersFoundCount; i++) {
      const userRow = this.usersTableRows.nth(i);

      const nameColumn = userRow.locator('td:nth-of-type(1)');
      const name = await nameColumn
        .locator('a[href^="/admin/users/"] span')
        .innerText();
      const email = await nameColumn
        .locator('a[href^="mailto:"] span')
        .innerText();

      const projectsColumn = userRow.locator('td:nth-of-type(2)');
      const projectsCount = Number(
        await projectsColumn
          .locator('div[data-testid^="user-project-count-"]')
          .innerText(),
      );

      const groupsColumn = userRow.locator('td:nth-of-type(3)');
      const groupsCount = Number(
        await groupsColumn
          .locator('[data-testid^="user-group-count-"] span')
          .innerText(),
      );

      const createdOnColumn = userRow.locator('td:nth-of-type(4)');
      const createdOnDate = await createdOnColumn.locator('span').innerText();

      const lastActivityColumn = userRow.locator('td:nth-of-type(5)');
      const lastActivity = await lastActivityColumn.locator('span').innerText();

      usersFound.push({
        name,
        email,
        projectsCount,
        groupsCount,
        created_at: createdOnDate,
        last_activity_on: lastActivity,
      } satisfies UserDashboardData);
    }
    return usersFound;
  }

  async getCurrentSortingName() {
    const currentSorting = await this.currentSortingButton.textContent();
    if (!currentSorting)
      throw new Error('Was not able to find the current sorting name!');
    return currentSorting;
  }
}
