import { type Locator, type Page } from '@playwright/test';

import { normalizeText } from '../../../utils/texts';

import { User } from '../../../models/User';

export class UserDetails {
  readonly page: Page;

  readonly editActionsDropdown: Locator;

  readonly editActionsComponent: Locator;

  readonly confirmUserBlockButton: Locator;

  readonly confirmUserBanButton: Locator;

  readonly confirmUserUnbanButton: Locator;

  readonly confirmUserDeactivateButton: Locator;

  readonly confirmUserActivateButton: Locator;

  readonly confirmUserDeleteButton: Locator;

  readonly confirmUsernameToDeleteCode: Locator;

  readonly confirmUsernameToDeleteInput: Locator;

  readonly flashAlert: Locator;

  /*Account tab*/
  readonly accountsTabSectionHeaders: Locator;

  readonly accountsTabItems: Locator;

  readonly mainNameHeader: Locator;
  /*Account tab*/

  constructor(page: Page) {
    this.page = page;
    this.accountsTabSectionHeaders = this.page.locator('.gl-card-header');
    this.accountsTabItems = this.page.locator('.content-list li');
    this.mainNameHeader = this.page.locator('h1');
    this.editActionsDropdown = this.page.locator(
      '[data-testid="user-actions-dropdown-toggle"] > button',
    );
    this.editActionsComponent = this.page.locator('[id^="disclosure-3"]');
    this.confirmUserBlockButton = this.page.getByRole('button', {
      name: 'Block',
    });
    this.confirmUserBanButton = this.page.getByRole('button', {
      name: 'Ban user',
    });
    this.confirmUserUnbanButton = this.page.getByRole('button', {
      name: 'Unban user',
    });
    this.confirmUserDeactivateButton = this.page.getByRole('button', {
      name: 'Deactivate',
    });
    this.confirmUserActivateButton = this.page.getByRole('button', {
      name: 'Activate',
    });
    this.confirmUserDeleteButton = this.page.getByRole('button', {
      name: 'Delete user',
    });
    this.confirmUsernameToDeleteCode = this.page.locator(
      '[data-testid="confirm-username"]',
    );
    this.confirmUsernameToDeleteInput = this.page.locator(
      'input[name="username"]',
    );
    this.flashAlert = this.page.locator('.gl-alert-body');
  }

  async visit({ username }: User) {
    await this.page.goto(`/admin/users/${username}`);
  }

  /*Account tab*/
  async getNameSectionTitle() {
    const sectionTitleText = await this.accountsTabSectionHeaders
      .nth(0)
      .textContent();
    if (!sectionTitleText) throw new Error('Text not found!!');
    return normalizeText(sectionTitleText);
  }

  async getAccountsTabItems() {
    const accountsTabItemsTexts = await this.accountsTabItems.allTextContents();
    return accountsTabItemsTexts.map((tabItemText) => {
      if (!tabItemText) throw new Error('Text not found!!');
      return normalizeText(tabItemText);
    });
  }

  async getMainNameHeader() {
    const mainHeaderText = await this.mainNameHeader.textContent();
    if (!mainHeaderText) throw new Error('Text not found!!');
    return normalizeText(mainHeaderText);
  }
  /*Account tab*/

  async pickAction(action: EditAction) {
    await this.editActionsDropdown.click();
    await this.editActionsComponent
      .getByRole('listitem')
      .getByText(`${action}`, { exact: true })
      .click();
  }

  async confirmUserBlock() {
    await this.confirmUserBlockButton.click();
  }

  async confirmUserBan() {
    await this.confirmUserBanButton.click();
  }

  async confirmUserUnban() {
    await this.confirmUserBanButton.click();
  }

  async confirmUserDeactivate() {
    await this.confirmUserDeactivateButton.click();
  }

  async confirmUserActivate() {
    await this.confirmUserActivateButton.click();
  }

  async confirmUserDelete() {
    const confirmUsername =
      (await this.confirmUsernameToDeleteCode.textContent())!;
    await this.confirmUsernameToDeleteInput.fill(confirmUsername);
    await this.confirmUserDeleteButton.click();
  }

  async getAllFlashAlertsContents() {
    const alerts = await this.flashAlert.allTextContents();
    return alerts.map((alertText) => {
      if (!alertText) throw new Error('Text not found!!');
      return normalizeText(alertText);
    });
  }
}

type EditAction =
  | 'Block'
  | 'Deactivate'
  | 'Activate'
  | 'Ban user'
  | 'Unban user'
  | 'Delete user'
  | 'Delete user and contributions';
