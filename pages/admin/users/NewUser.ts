import { type Locator, type Page } from '@playwright/test';

import { User } from '../../../models/User';

export class NewUser {
  readonly page: Page;

  readonly accountNameInput: Locator;

  readonly accountUsernameInput: Locator;

  readonly accountEmailInput: Locator;

  readonly createUserButton: Locator;

  readonly accessLevelRegularRadio: Locator;

  readonly accessLevelAdministratorRadio: Locator;

  constructor(page: Page) {
    this.page = page;
    this.accountNameInput = this.page.locator('#user_name');
    this.accountUsernameInput = this.page.locator('#user_username');
    this.accountEmailInput = this.page.locator('#user_email');
    this.createUserButton = this.page.locator('button[type="submit"]');
    this.accessLevelRegularRadio = this.page.locator(
      'input#user_access_level_regular + label',
    );
    this.accessLevelAdministratorRadio = this.page.locator(
      'input#user_access_level_admin + label',
    );
  }

  async fillAccountData({ name, username, email }: User) {
    await this.accountNameInput.fill(name);
    await this.accountUsernameInput.fill(username);
    await this.accountEmailInput.fill(email);
  }

  async createUser() {
    await this.createUserButton.click();
  }

  async setAccessLevel(level: AccessLevel) {
    switch (level) {
      case 'Administrator':
        if (!(await this.accessLevelAdministratorRadio.isChecked()))
          await this.accessLevelAdministratorRadio.click();
        break;
      case 'Regular':
        if (!(await this.accessLevelRegularRadio.isChecked()))
          await this.accessLevelRegularRadio.click();
        break;
    }
  }
}

type AccessLevel = 'Administrator' | 'Regular';
