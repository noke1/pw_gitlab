import { type Locator, type Page } from '@playwright/test';

import { ProjectDashboardData } from '../../../models/Project';

export class AdminProjectsDashboard {
  readonly page: Page;

  readonly projectsTableRows: Locator;

  readonly filterByNameInput: Locator;

  constructor(page: Page) {
    this.page = page;
    this.projectsTableRows = this.page.locator('ul.content-list');
    this.filterByNameInput = this.page.locator('#project-filter-form-field');
  }

  async visit() {
    const getProjects = this.page.waitForResponse('**/admin/projects');
    await this.page.goto('/admin/projects');
    await getProjects;
  }

  async filterByName(filterPhrase: string) {
    const filterResults = this.page.waitForResponse((response) =>
      response.url().includes('admin/projects.json'),
    );
    await this.filterByNameInput.fill(filterPhrase);
    await filterResults;
  }

  async getTableRowsContent() {
    const projectsFound: ProjectDashboardData[] = [];
    const projectsFoundCount = await this.projectsTableRows.count();
    for (let i = 0; i < projectsFoundCount; i++) {
      const projectRow = this.projectsTableRows.nth(i);

      const projectNamespace = await projectRow
        .locator('span.namespace-name')
        .innerText();
      const projectName = await projectRow
        .locator('span.project-name')
        .innerText();

      projectsFound.push({
        name: projectName,
        name_with_namespace: projectNamespace,
      } satisfies ProjectDashboardData);
    }
    return projectsFound;
  }
}
