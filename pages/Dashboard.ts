import { type Locator, type Page } from '@playwright/test';

export class Dashboard {
  readonly page: Page;

  readonly welcomeMessage: Locator;

  constructor(page: Page) {
    this.page = page;
    this.welcomeMessage = page.locator('[data-testid="welcome-title-content"]');
  }
}
