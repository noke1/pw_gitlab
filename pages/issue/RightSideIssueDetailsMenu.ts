import { type Locator, type Page, expect } from '@playwright/test';

export class RightSideIssueDetailsMenu {
  readonly page: Page;

  readonly editLabelsSection: Locator;

  readonly selectedLabels: Locator;

  constructor(page: Page) {
    this.page = page;
    this.editLabelsSection = this.page.locator('[data-testid="sidebar-labels"]');
    this.selectedLabels = this.page.locator(
      '[data-testid="collapsed-content"] [data-testid="selected-label-content"]',
    );
  }

  async expandLabelsEditor() {
    await this.editLabelsSection
      .locator('[data-testid="edit-button"] span')
      .click();
  }

  async createNewLabel(labelName: string, labelColor: string) {
    await this.editLabelsSection
      .locator('[data-testid="create-label-button"]')
      .click();
    await this.editLabelsSection
      .locator('[data-testid="label-title-input"]')
      .fill(labelName);
    const labelColorInput = this.editLabelsSection.locator(
      '[data-testid="selected-color-text"]',
    );
    await labelColorInput.clear();
    await labelColorInput.fill(labelColor);
    const createLabelResponse = this.page.waitForResponse('**/graphql');
    const createLabelButton = this.editLabelsSection.locator(
      '[data-testid="create-button"]',
    );
    expect(await createLabelButton.getAttribute('class')).not.toContain(
      'disabled',
    );
    await createLabelButton.click();
    await createLabelResponse;
    await expect(
      this.editLabelsSection.locator('[data-testid="label-color-box"] + span', {
        hasText: labelName,
      }),
    ).toBeVisible();
    await this.editLabelsSection
      .locator('[data-testid="close-icon"] use')
      .click();
  }

  async getSelectedLabelsNames() {
    await this.selectedLabels.locator('a span').waitFor();
    return this.selectedLabels.locator('a span').allTextContents();
  }
}
