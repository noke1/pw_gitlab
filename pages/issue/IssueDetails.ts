import { type Locator, type Page, expect } from '@playwright/test';

export class IssueDetails {
  readonly page: Page;

  readonly issueTitleText: Locator;

  readonly issueStatusText: Locator;

  readonly commentTextArea: Locator;

  readonly submitCommentButton: Locator;

  readonly richTextPopup: Locator;

  readonly activitiesList: Locator;

  readonly reactions: Locator;

  readonly addReactionButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.issueTitleText = this.page.getByTestId('issue-title');
    this.issueStatusText = this.page.locator(
      '[data-testid="issue-state-badge"] span',
    );
    this.commentTextArea = this.page.locator('[data-testid="comment-field"]');
    this.submitCommentButton = this.page.getByTestId('comment-button');
    this.richTextPopup = this.page.locator(
      '[data-testid="rich-text-promo-popover"]',
    );
    this.activitiesList = this.page.locator('#notes-list > li');
    this.reactions = this.page.locator('[data-testid="award-button"]');
    this.addReactionButton = this.page.locator('[data-testid="emoji-picker"]');
  }

  async visit(username: string, projectname: string, issueId: number) {
    await this.page.goto(`/${username}/${projectname}/-/issues/${issueId}`);
  }

  async getIssueTitle() {
    return this.issueTitleText.textContent();
  }

  async getIssueStatus() {
    return this.issueStatusText.textContent();
  }

  async addComment(text: string) {
    if (await this.richTextPopup.isVisible()) {
      await this.richTextPopup
        .first()
        .locator('[data-testid="close-button"]')
        .click();
    }
    await this.commentTextArea.fill(text);
    await this.submitCommentButton.click();
  }

  async getActivitiesList() {
    const activitiesCount = await this.activitiesList.count();
    expect(
      activitiesCount,
      'The activities list should not be empty!!',
    ).toBeGreaterThan(0);
    return this.activitiesList;
  }

  async getActivitiesTextContent() {
    return this.activitiesList.locator('p[dir="auto"]').allTextContents();
  }

  async addReaction(reactionName: ReactionName) {
    await this.addReactionButton.click();

    const response = this.page.waitForResponse('**/realtime_changes');
    await this.addReactionButton
      .locator('input')
      .pressSequentially(reactionName);
    //Those two selectors are to make the test less flaky. The playwirght is kind to fast searching for the elements sometimes
    await this.page.waitForSelector('[data-testid="clear-icon"]');
    await this.page.waitForSelector('[role="menu"] b:has-text("Search")');
    await response;
    const reactionsFound: Locator = this.addReactionButton.locator(
      '[data-testid="emoji-button"]',
    );

    const reactionsFoundCount: number = await reactionsFound.count();
    for (let i = 0; i < reactionsFoundCount; i++) {
      const currentReaction: Locator = reactionsFound.nth(i).locator('gl-emoji');
      const currentReactionName = (await currentReaction.getAttribute(
        'data-name',
      )) as string;

      if (reactionName === currentReactionName) {
        const addReaction = this.page.waitForResponse('**/award_emoji');
        await currentReaction.click();
        await addReaction;
      }
    }
  }

  async clickReaction(reactionName: ReactionName) {
    const clickReactionResponse = this.page.waitForResponse(
      (response) =>
        response.url().includes('/award_emoji') &&
        (response.status() === 201 || response.status() === 204),
    );
    await this.reactions
      .locator(`gl-emoji[data-name='${reactionName}']`)
      .click();
    await clickReactionResponse;
  }

  async getCurrentReactions() {
    const reactions = await this.reactions.count();

    const currentReactions: Record<string, number> = {};
    for (let i = 0; i < reactions; i++) {
      const reactionName = (await this.reactions
        .nth(i)
        .locator('gl-emoji')
        .getAttribute('data-name')) as string;
      const reactionsCount = Number(
        (await this.reactions
          .nth(i)
          .locator('span.js-counter')
          .textContent()) as string,
      );

      currentReactions[reactionName] = reactionsCount;
    }
    return currentReactions;
  }
}

type ReactionName = 'thumbsup' | 'thumbsdown' | 'swimmer_tone5';
