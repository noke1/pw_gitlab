import { type Locator, type Page } from '@playwright/test';
import { IssueMinimumDataUiModel } from '../../models/Issue';

export class NewIssue {
  readonly page: Page;

  readonly issueTitleInput: Locator;

  readonly issueTypeDropdown: Locator;

  readonly issueTypeTypesList: Locator;

  readonly createIssueButton: Locator;

  readonly richTextPopup: Locator;

  constructor(page: Page) {
    this.page = page;
    this.issueTitleInput = this.page.locator('input#issue_title');
    this.issueTypeDropdown = this.page
      .locator('label[for="issue_type"] + div button')
      .first();
    this.issueTypeTypesList = this.page.locator('ul li[role="option"] span');
    this.createIssueButton = this.page.locator(
      '[data-testid="issuable-create-button"]',
    );
    this.richTextPopup = this.page.locator(
      '[data-testid="rich-text-promo-popover"]',
    );
  }

  async visit(username: string, projectName: string) {
    await this.page.goto(`/${username}/${projectName}/-/issues/new`);
  }

  async fillWithMiniumData(
    projectData: IssueMinimumDataUiModel,
  ) {
    const isThatFckingAnnoyingPopUpVisible = await this.richTextPopup.isVisible(
      { timeout: 5_000 },
    );
    if (isThatFckingAnnoyingPopUpVisible) {
      await this.richTextPopup
        .first()
        .locator('[data-testid="close-button"]')
        .click();
    }
    await this.issueTitleInput.fill(projectData.title);
    await this.issueTypeDropdown.click();
    await this.issueTypeTypesList.getByText(projectData.issue_type!).click();
  }

  async createIssue() {
    const createIssueResponse = this.page.waitForResponse('**/issues');
    await this.createIssueButton.click();
    await createIssueResponse;
  }
}
