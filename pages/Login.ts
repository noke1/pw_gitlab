import { type Locator, type Page } from '@playwright/test';

import { UserLogin } from '../models/User';

export class Login {
  readonly page: Page;

  readonly usernameInput: Locator;

  readonly passwordInput: Locator;

  readonly logInButton: Locator;

  readonly errorMessageAlert: Locator;

  constructor(page: Page) {
    this.page = page;
    this.usernameInput = page.getByTestId('username-field');
    this.passwordInput = page.getByTestId('password-field');
    this.logInButton = page.getByTestId('sign-in-button');
    this.errorMessageAlert = page.locator('[data-testid="alert-danger"]');
  }

  async visit() {
    await this.page.goto('/');
  }

  async as(user: UserLogin) {
    await this.usernameInput.fill(user.username);
    await this.passwordInput.fill(user.password);
    await this.logInButton.click();
  }

  async getErrorMessageAlertText() {
    return (await this.errorMessageAlert.textContent())!;
  }
}
