#!/bin/bash

URL=$1
SUFFIX="/users/sign_in"
PHRASE="Username or primary email"
COUNTER=1
MAX_ATTEMPTS=50
DELAY=10

while true; do
	if [ $COUNTER -ge $MAX_ATTEMPTS ]; then
		echo "!!! Max attempts of $MAX_ATTEMPTS reached. Closing the script. !!!"
		exit 1
	fi

	echo ">>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
	echo ">>> Attempt: $COUNTER out of $MAX_ATTEMPTS <<<"
	echo ">>> Requesting the $URL$SUFFIX... <<<"

	response=$(curl -s --max-time 10 "$URL$SUFFIX")  # Added a 10-second timeout

	STATUS=$(curl -o /dev/null -s -w "%{http_code}\n" "$URL$SUFFIX")  # Moved the status check here

	if [[ "$response" == *"$PHRASE"* ]] || [ "$STATUS" -eq 200 ]; then
		echo "!!! I got the redirection response with a status of 200 !!!"
		echo "!!! Ending the script !!!"
		exit 0
	else
		echo ">>> Response does not contain the given string or has a non-200 status. Trying once more in $DELAY seconds! <<<"
		echo ">>> The response status was: $STATUS"
		COUNTER=$((COUNTER + 1))
	fi

	echo ">>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
	sleep $DELAY
done
