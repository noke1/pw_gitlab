import { expect } from '@playwright/test';
import { test } from '../fixtures/global-fixture';

import { UserProvider } from '../providers/UserProvider';

import { normalizeText } from '../utils/texts';

test.describe('Authentication activities', async () => {
  test('@mobile Login as admin user', async ({
    login,
    dashboard,
    getTranslations,
    setupUser,
  }) => {
    const userData: User = new UserProvider().generateRandomAdmin();
    await setupUser(userData);
    await login.visit();

    await login.as({
      username: userData.username,
      password: userData.password,
    });

    await expect(dashboard.welcomeMessage).toHaveText(
      getTranslations().dashboard.welcomeMessage,
    );
  });

  test(
    'Login as regular user',
    { tag: ['@smoke'] },
    async ({ login, dashboard, getTranslations, setupUser }) => {
      const userData: User = new UserProvider().generateRandomRegularUser();
      await setupUser(userData);
      await login.visit();

      await login.as({
        username: userData.username,
        password: userData.password,
      });

      await expect(dashboard.welcomeMessage).toHaveText(
        getTranslations().dashboard.welcomeMessage,
      );
    },
  );

  test('Logout as admin user', async ({ login, leftSideMenu, setupUser }) => {
    const userData: User = new UserProvider().generateRandomAdmin();
    await setupUser(userData);
    await login.visit();
    await login.as({
      username: userData.username,
      password: userData.password,
    });

    await leftSideMenu.signOut();

    await expect(login.logInButton).toBeVisible();
  });

  test(
    'Logout as regular user',
    { tag: ['@smoke'] },
    async ({ login, leftSideMenu, setupUser }) => {
      const userData: User = new UserProvider().generateRandomRegularUser();
      await setupUser(userData);
      await login.visit();
      await login.as({
        username: userData.username,
        password: userData.password,
      });

      await leftSideMenu.signOut();

      await expect(login.logInButton).toBeVisible();
    },
  );

  test('Banned user cannot login with the specific error message', async ({
    login,
    banUser,
    getTranslations,
    setupUser,
  }) => {
    const userData: User = new UserProvider().generateRandomRegularUser();
    const userId = await setupUser(userData);

    await banUser(userId);
    await login.visit();
    await login.as({
      username: userData.username,
      password: userData.password,
    });

    const banMessage = await login.getErrorMessageAlertText();
    expect(normalizeText(banMessage)).toEqual(
      getTranslations().login.banUserMessage,
    );
  });
});
