import { test } from '../../fixtures/global-fixture';
import { expect } from '../../matchers/sorting-matchers';

import { UserProvider } from '../../providers/UserProvider';

import { ProjectProvider } from '../../providers/ProjectProvider';

import { User } from '../../models/User';

test.describe('Projects table', () => {
  let userData: User;

  test.beforeEach(async ({ setupUser, login }) => {
    userData = new UserProvider().generateRandomAdmin();
    await setupUser(userData);
    await login.visit();
    await login.as({
      username: userData.username,
      password: userData.password,
    });
  });

  test('Can filter the project by its name and is visible in results', async ({
    setupProject,
    adminProjectsDashboard,
  }) => {
    const projectData = new ProjectProvider().generateRandomPublicProject();
    await setupProject(projectData);
    await adminProjectsDashboard.visit();

    await adminProjectsDashboard.filterByName(projectData.name);
    const projectsFound = await adminProjectsDashboard.getTableRowsContent();

    expect(projectsFound.length).toBeGreaterThanOrEqual(1);
    expect(projectsFound.map((project) => project.name)).toEqual([
      projectData.name,
    ]);
  });
});
