import { normalizeText } from '../../utils/texts';
import { test } from '../../fixtures/global-fixture';
import { expect } from '../../matchers/sorting-matchers';

import { UserProvider } from '../../providers/UserProvider';

import { CallContext } from '../../api/CallContext';
import { UserResource } from '../../api/UserResource';
import { UsersDashboard } from '../../pages/admin/users/UsersDashboard';
import { User, UserDashboardData } from '../../models/User';

test.describe('User creation', () => {
  test.beforeEach(async ({ login, usersDashboard }) => {
    const adminData: User = new UserProvider().generateRandomAdmin();
    const asAdmin: CallContext = new CallContext();
    const userResource: UserResource = new UserResource(asAdmin);
    await userResource.createUser(adminData);
    await login.visit();

    await login.as({
      username: adminData.username,
      password: adminData.password,
    });
    await usersDashboard.visit();
  });

  test(
    'Admin can create regular user with minimum required data',
    { tag: ['@smoke'] },
    async ({ usersDashboard, userDetails, newUser }) => {
      const regularUserData: User =
        new UserProvider().generateRandomRegularUser();

      await usersDashboard.goToNewUser();
      await newUser.fillAccountData(regularUserData);
      await newUser.setAccessLevel('Regular');
      await newUser.createUser();

      const title = await userDetails.getNameSectionTitle();
      const items = await userDetails.getAccountsTabItems();
      expect(title).toBe(regularUserData.name);
      expect(items).toContain(`Profile page:${regularUserData.username}`);
      expect(items).toContain(`Name:${regularUserData.name}`);
      expect(items).toContain(`Username:${regularUserData.username}`);
      expect(items).toContain(
        `Email:${regularUserData.email.toLowerCase()}Verified`,
      );
    },
  );

  test('Admin can create administrator with minimum required data', async ({
    usersDashboard,
    userDetails,
    newUser,
  }) => {
    const adminUserData: User = new UserProvider().generateRandomAdmin();

    await usersDashboard.goToNewUser();
    await newUser.fillAccountData(adminUserData);
    await newUser.setAccessLevel('Administrator');
    await newUser.createUser();

    const name = await userDetails.getMainNameHeader();
    const title = await userDetails.getNameSectionTitle();
    const items = await userDetails.getAccountsTabItems();
    expect(name).toBe(`${adminUserData.name}(Admin)`);
    expect(title).toBe(adminUserData.name);
    expect(items).toContain(`Profile page:${adminUserData.username}`);
    expect(items).toContain(`Name:${adminUserData.name}`);
    expect(items).toContain(`Username:${adminUserData.username}`);
    expect(items).toContain(
      `Email:${adminUserData.email.toLowerCase()}Verified`,
    );
  });
});

test.describe('Edit user', () => {
  test.beforeEach(async ({ login, usersDashboard }) => {
    const adminData: User = new UserProvider().generateRandomAdmin();
    const asAdmin: CallContext = new CallContext();
    const userResource: UserResource = new UserResource(asAdmin);
    await userResource.createUser(adminData);
    await login.visit();

    await login.as({
      username: adminData.username,
      password: adminData.password,
    });
    await usersDashboard.visit();
  });

  test(
    'Administrator can block a regular user',
    { tag: ['@smoke'] },
    async ({ userDetails, setupUser }) => {
      const regularUserData: User =
        new UserProvider().generateRandomRegularUser();
      await setupUser(regularUserData);
      await userDetails.visit(regularUserData);
      const nameBeforeBlock = await userDetails.getMainNameHeader();
      expect(nameBeforeBlock).toBe(regularUserData.name);

      await userDetails.pickAction('Block');
      await userDetails.confirmUserBlock();

      const nameAfterBlock = await userDetails.getMainNameHeader();
      expect(nameAfterBlock).toBe(`${regularUserData.name}(Blocked)`);
      const alerts = await userDetails.getAllFlashAlertsContents();
      expect(alerts).toContain('Successfully blocked');
    },
  );

  test('Administrator can ban a regular user', async ({
    userDetails,
    setupUser,
  }) => {
    const regularUserData: User = new UserProvider().generateRandomRegularUser();
    await setupUser(regularUserData);
    await userDetails.visit(regularUserData);
    const nameBeforeBan = await userDetails.getMainNameHeader();
    expect(nameBeforeBan).toBe(regularUserData.name);

    await userDetails.pickAction('Ban user');
    await userDetails.confirmUserBan();

    const nameAfterBan = await userDetails.getMainNameHeader();
    expect(nameAfterBan).toBe(`${regularUserData.name}(Banned)`);
    const alerts = await userDetails.getAllFlashAlertsContents();
    expect(alerts).toContain('Successfully banned');
  });

  test('Administrator can unban a regular user', async ({
    userDetails,
    setupUser,
    banUser,
  }) => {
    const regularUserData: User = new UserProvider().generateRandomRegularUser();
    const userId = await setupUser(regularUserData);
    await banUser(userId);
    await userDetails.visit(regularUserData);
    const nameBeforeUnban = await userDetails.getMainNameHeader();
    expect(nameBeforeUnban).toBe(`${regularUserData.name}(Banned)`);

    await userDetails.pickAction('Unban user');
    await userDetails.confirmUserBan();

    const nameAfterUnban = await userDetails.getMainNameHeader();
    expect(nameAfterUnban).toBe(`${regularUserData.name}`);
    const alerts = await userDetails.getAllFlashAlertsContents();
    expect(alerts).toContain('Successfully unbanned');
  });

  test('Administrator can deactivate a regular user', async ({
    userDetails,
    setupUser,
  }) => {
    const regularUserData: User = new UserProvider().generateRandomRegularUser();
    await setupUser(regularUserData);
    await userDetails.visit(regularUserData);
    const nameBeforeDeactivate = await userDetails.getMainNameHeader();
    expect(nameBeforeDeactivate).toBe(regularUserData.name);

    await userDetails.pickAction('Deactivate');
    await userDetails.confirmUserDeactivate();

    const nameAfterDeactivate = await userDetails.getMainNameHeader();
    expect(nameAfterDeactivate).toBe(`${regularUserData.name}(Deactivated)`);
    const alerts = await userDetails.getAllFlashAlertsContents();
    expect(alerts).toContain('Successfully deactivated');
  });

  test('Administrator can activate a regular user', async ({
    userDetails,
    setupUser,
    deactivateUser,
  }) => {
    const regularUserData: User = new UserProvider().generateRandomRegularUser();
    const userId = await setupUser(regularUserData);
    await deactivateUser(userId);
    await userDetails.visit(regularUserData);
    const nameBeforeActivate = await userDetails.getMainNameHeader();
    expect(nameBeforeActivate).toBe(`${regularUserData.name}(Deactivated)`);

    await userDetails.pickAction('Activate');
    await userDetails.confirmUserActivate();

    const nameAfterActivate = await userDetails.getMainNameHeader();
    expect(nameAfterActivate).toBe(`${regularUserData.name}`);
    const alerts = await userDetails.getAllFlashAlertsContents();
    expect(alerts).toContain('Successfully activated');
  });

  test(
    'Administrator can delete a regular user',
    { tag: ['@smoke'] },
    async ({ userDetails, usersDashboard, setupUser }) => {
      const regularUserData: User =
        new UserProvider().generateRandomRegularUser();
      await setupUser(regularUserData);
      await userDetails.visit(regularUserData);

      await userDetails.pickAction('Delete user');
      await userDetails.confirmUserDelete();

      const alerts = await usersDashboard.getAlertsTexts();
      expect(alerts).toContain('The user is being deleted.');
    },
  );
});

test.describe('Users Dashboard table', () => {
  test.beforeEach(async ({ login, usersDashboard }) => {
    const adminData: User = new UserProvider().generateRandomAdmin();
    const asAdmin: CallContext = new CallContext();
    const userResource: UserResource = new UserResource(asAdmin);
    await userResource.createUser(adminData);
    await login.visit();

    await login.as({
      username: adminData.username,
      password: adminData.password,
    });
    await usersDashboard.visit();
  });

  const goThroughAllPages = async (
    usersDashboard: UsersDashboard,
    maxTableRows: number,
  ) => {
    const isNextPageButtonDisabled =
      await usersDashboard.isButtonNextPageDisabled();
  
    if (!isNextPageButtonDisabled) {
      const actualRowsFoundCount = await usersDashboard.usersTableRows.count();
      expect(actualRowsFoundCount).toEqual(maxTableRows);
      await usersDashboard.goToNextPage();
      await goThroughAllPages(usersDashboard, maxTableRows);
    } else {
      const rowsCount = await usersDashboard.usersTableRows.count();
      expect(rowsCount).toBeLessThanOrEqual(maxTableRows);
      console.log('🛑Navigated to the last page, test ends!!🛑');
    }
  };
  test('All pages got the same number of results and can navigate through each of them', async ({
    usersDashboard,
    setupUser,
    deleteUser,
  }) => {
    test.setTimeout(100_000);

    const maxTableRows = 20;
    const createdUsersUserIds: number[] = [];
    const setupUserPromises: Promise<number>[] = Array.from(
      { length: maxTableRows + 1 },
      () => {
        const temp = new UserProvider().generateRandomRegularUser();
        return setupUser(temp);
      },
    );
    const userIds = await Promise.all(setupUserPromises);
    createdUsersUserIds.push(...userIds);

    await goThroughAllPages(usersDashboard, maxTableRows);

    const deleteUserPromises: Promise<void>[] = createdUsersUserIds.map((id) =>
      deleteUser(id),
    );
    await Promise.all(deleteUserPromises);
  });

  test('Search by name returns only matching results', async ({
    usersDashboard,
    setupUser,
  }) => {
    const userToSearchFor: User = new UserProvider().generateRandomRegularUser();
    await setupUser(userToSearchFor);

    await usersDashboard.searchForUser(userToSearchFor.name);
    const usersFound = await usersDashboard.getTableRowsContent();

    expect(usersFound.length).toBeGreaterThanOrEqual(1);
    const fullUsernames: string[] = usersFound.map((user) => user.name);
    fullUsernames.forEach((fullUsername) => {
      const names: string[] = userToSearchFor.name.split(' ');
      names.forEach((namePart) => {
        expect(fullUsername).toContain(namePart);
      });
    });
  });

  test('Default sorting by name ascending when entering the screen works', async ({
    usersDashboard,
    setupUser,
    deleteUser,
  }) => {
    const numberOfUsersToCreate: number = 10;
    const createdUsersUserIds: number[] = [];
    for (let i = 0; i < numberOfUsersToCreate; i++) {
      const temp = new UserProvider().generateRandomRegularUser();
      const userId: number = await setupUser(temp);
      createdUsersUserIds.push(userId);
    }

    const currentSorting: string = normalizeText(
      await usersDashboard.getCurrentSortingName(),
    );
    const usersFound: UserDashboardData[] =
      await usersDashboard.getTableRowsContent();

    expect(currentSorting).toEqual('Name');
    expect(usersFound.length).toBeGreaterThanOrEqual(1);
    const fullUsernames: string[] = usersFound.map((user) => user.name);
    expect(fullUsernames).toBeSorted({ direction: 'asc' });

    for (const id of createdUsersUserIds) await deleteUser(id);
  });
});