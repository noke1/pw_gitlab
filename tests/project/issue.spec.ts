import { UserProvider } from '../../providers/UserProvider';
import { ProjectProvider } from '../../providers/ProjectProvider';

import { test } from '../../fixtures/global-fixture';

import { expect } from '@playwright/test';

import { User } from '../../models/User';
import { MemberRequest } from '../../models/Member';
import { PersonalAccessTokenRequest } from '../../models/PersonalAccessToken';
import { IssueMinimumDataUiModel, IssueRequest } from '../../models/Issue';
import { ProjectRequest } from '../../models/Project';

test.describe('Create issue', () => {
  let userData: User;
  let projectData: ProjectRequest;

  test.beforeEach(
    async ({
      setupUser,
      setupProject,
      login,
      projectDashboard,
      setupAccessToken,
    }) => {
      userData = new UserProvider().generateRandomRegularUser();
      projectData = new ProjectProvider().generateRandomPublicProject();

      const userId: number = await setupUser(userData);
      const tokenData = {
        name: 'API_TOKEN_TEST',
        user_id: userId,
        scopes: ['api'],
      } satisfies PersonalAccessTokenRequest;
      const userAccessToken: string = await setupAccessToken(tokenData);
      await setupProject(projectData, userAccessToken);
      await login.visit();
      await login.as({
        username: userData.username,
        password: userData.password,
      });
      await projectDashboard.visit(userData.username, projectData.name);
    },
  );

  test(
    'Can create issue with minimal required data',
    { tag: ['@smoke'] },
    async ({
      leftSideMenu,
      projectDashboard,
      newIssue,
      issueDetails,
      getTranslations,
    }) => {
      const issueData = {
        title: 'TEST Issue XYZ',
        issue_type: 'Issue',
      } satisfies IssueMinimumDataUiModel;
      await leftSideMenu.goToIssues();
      await projectDashboard.clickNewIssue();

      await newIssue.fillWithMiniumData(issueData);
      await newIssue.createIssue();

      expect(
        await issueDetails.getIssueTitle(),
        'Issue title should match the title that was provided',
      ).toEqual(issueData.title);
      const openStatusName =
        getTranslations().issue.issueDetails.issueOpenStatus;
      expect(
        await issueDetails.getIssueStatus(),
        `Newly created issue should have ${openStatusName} status`,
      ).toEqual(openStatusName);
    },
  );

  test('Can create incident with minimal required data', async ({
    leftSideMenu,
    projectDashboard,
    newIssue,
    issueDetails,
    getTranslations,
  }) => {
    const incidentData = {
      title: 'TEST Issue XYZ',
      issue_type: 'Incident',
    } satisfies IssueMinimumDataUiModel;
    await leftSideMenu.goToIssues();
    await projectDashboard.clickNewIssue();

    await newIssue.fillWithMiniumData(incidentData);
    await newIssue.createIssue();

    expect(
      await issueDetails.getIssueTitle(),
      'Incident title should match the title that was provided',
    ).toEqual(incidentData.title);
    const openStatusName = getTranslations().issue.issueDetails.issueOpenStatus;
    expect(
      await issueDetails.getIssueStatus(),
      `Newly created incident should have ${openStatusName} status`,
    ).toEqual(openStatusName);
  });
});

test.describe('Edit issue', () => {
  let userData: User;
  let projectData: ProjectRequest;
  let issueId: number;
  let projectId: number;

  test.beforeEach(
    async ({ setupUser, setupAccessToken, setupProject, setupIssue }) => {
      userData = new UserProvider().generateRandomRegularUser();
      projectData = new ProjectProvider().generateRandomPublicProject();

      const userId: number = await setupUser(userData);
      const tokenData = {
        name: 'API_TOKEN_TEST',
        user_id: userId,
        scopes: ['api'],
      } satisfies PersonalAccessTokenRequest;
      const userAccessToken: string = await setupAccessToken(tokenData);
      projectId = await setupProject(projectData, userAccessToken);
      const issueData = {
        id: projectId,
        title: 'API Issue Test 123',
      } satisfies IssueRequest;
      issueId = await setupIssue(issueData, projectId, userAccessToken);
    },
  );

  test('Can add a comment to an issue', async ({ login, issueDetails }) => {
    await login.visit();
    await login.as({
      username: userData.username,
      password: userData.password,
    });
    await issueDetails.visit(userData.username, projectData.name, issueId);
    await issueDetails.addComment('Test comment 1');
    await issueDetails.addComment('Test comment 2');

    const activities = await issueDetails.getActivitiesList();
    const activitiesTextContent = await issueDetails.getActivitiesTextContent();
    expect(await activities.count()).toEqual(2);
    expect(activitiesTextContent).toEqual(['Test comment 1', 'Test comment 2']);
  });

  test('Another user can add a new label to the issue', async ({
    setupUser,
    setupUserInProject,
    login,
    issueDetails,
    rightSideIssueDetailsMenu,
  }) => {
    const anotherUserData: User = new UserProvider().generateRandomRegularUser();
    const anotherUserId: number = await setupUser(anotherUserData);
    const memberData = {
      id: projectId,
      access_level: 30,
      user_id: anotherUserId,
    } satisfies MemberRequest;
    await setupUserInProject(projectId, memberData);
    await login.visit();
    await login.as({
      username: anotherUserData.username,
      password: anotherUserData.password,
    });
    await issueDetails.visit(userData.username, projectData.name, issueId);

    await rightSideIssueDetailsMenu.expandLabelsEditor();
    await rightSideIssueDetailsMenu.createNewLabel('test_label', '#cd5b45');
    const selectedLabels =
      await rightSideIssueDetailsMenu.getSelectedLabelsNames();

    expect(
      selectedLabels?.map((label: string) => label.trim().replace(/\n/g, '')),
    ).toStrictEqual(['test_label']);
  });

  test('Can upvote, downvote, add custom reaction', async ({
    login,
    issueDetails,
  }) => {
    await login.visit();
    await login.as({
      username: userData.username,
      password: userData.password,
    });
    await issueDetails.visit(userData.username, projectData.name, issueId);

    let reactions = await issueDetails.getCurrentReactions();
    expect(
      reactions,
      'Current reactions name and counts should match the given one',
    ).toStrictEqual({ thumbsup: 0, thumbsdown: 0 });

    await issueDetails.clickReaction('thumbsup');
    await issueDetails.clickReaction('thumbsdown');
    reactions = await issueDetails.getCurrentReactions();
    expect(
      reactions,
      'Current reactions name and counts should match the given one',
    ).toStrictEqual({ thumbsup: 1, thumbsdown: 1 });

    await issueDetails.addReaction('swimmer_tone5');
    reactions = await issueDetails.getCurrentReactions();
    expect(
      reactions,
      'Current reactions name and counts should match the given one',
    ).toStrictEqual({
      thumbsup: 1,
      thumbsdown: 1,
      swimmer_tone5: 1,
    });

    await issueDetails.clickReaction('thumbsup');
    await issueDetails.clickReaction('thumbsdown');
    await issueDetails.clickReaction('swimmer_tone5');
    reactions = await issueDetails.getCurrentReactions();
    expect(
      reactions,
      'Current reactions name and counts should match the given one',
    ).toStrictEqual({
      thumbsup: 0,
      thumbsdown: 0,
    });
  });
});
