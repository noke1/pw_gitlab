import { UserProvider } from '../../providers/UserProvider';
import { ProjectProvider } from '../../providers/ProjectProvider';
import { ProjectMemberRow } from '../../pages/project/ProjectMembers';

import { test } from '../../fixtures/global-fixture';
import { expect } from '@playwright/test';
import moment from 'moment';

import { PersonalAccessTokenRequest } from '../../models/PersonalAccessToken';
import { User } from '../../models/User';
import { ProjectRequest } from '../../models/Project';

test.describe('Invitations', () => {
  let userData: User;
  let projectData: ProjectRequest;

  test.beforeEach(
    async ({
      setupUser,
      setupProject,
      login,
      projectDashboard,
      setupAccessToken,
    }) => {
      userData = new UserProvider().generateRandomRegularUser();
      projectData = new ProjectProvider().generateRandomPublicProject();

      const userId: number = await setupUser(userData);
      const tokenData = {
        name: 'API_TOKEN_TEST',
        user_id: userId,
        scopes: ['api'],
      } satisfies PersonalAccessTokenRequest;
      const userAccessToken: string = await setupAccessToken(tokenData);
      await setupProject(projectData, userAccessToken);
      await login.visit();
      await login.as({
        username: userData.username,
        password: userData.password,
      });
      await projectDashboard.visit(userData.username, projectData.name);
    },
  );

  test(
    'Can invite new member as a guest to the project',
    { tag: ['@smoke'] },
    async ({ leftSideMenu, projectMembers, setupUser }) => {
      const userToBeInvited = new UserProvider().generateRandomRegularUser();
      await setupUser(userToBeInvited);
      const memberData: ProjectMemberRow = {
        username: `@${userToBeInvited.username}`,
        role: 'Reporter',
        expirationDate: moment().add(3, 'months').format('YYYY-MM-DD'),
      };
      const option = await leftSideMenu.goToOption('Manage');
      await option('Members');

      await projectMembers.inviteMember(memberData);
      const currentProjectMembers = await projectMembers.getProjectMembers();
      const addedMember = currentProjectMembers.find(
        (x) => x.username === memberData.username,
      );

      expect(addedMember).toEqual({
        username: memberData.username,
        role: memberData.role,
        //According to Gitlab logic the date passed while inviting is a one-day less
        expirationDate: memberData.expirationDate,
      } satisfies ProjectMemberRow);
      await expect(projectMembers.alertDiv).toHaveText(
        'Members were successfully added',
      );
    },
  );
});
