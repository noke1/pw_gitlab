import { test, expect } from '@playwright/test';
import { normalizeText } from '../../utils/texts';

type TestData = {
  input: string
  output: string
  testName: string
};

test.describe('Normalize text tests', () => {
  const testData: TestData[] = [
    {
      input: '&nbsp;&nbsp;Test StringString&nbsp;',
      output: 'Test StringString',
      testName: 'Should remove all the &nbsp;',
    },
    {
      input: '   Test StringString    ',
      output: 'Test StringString',
      testName: 'Should trim the text',
    },
    {
      input: '\nTest StringString\n',
      output: 'Test StringString',
      testName: 'Should remove all the new line signs',
    },
    {
      input: 'Test String  String       Ssds',
      output: 'Test String String Ssds',
      testName:
        'Should remove remove all more than 1 consecutive spaces and leave one',
    },
    {
      input: 'Test StringString Ssds',
      output: 'Test StringString Ssds',
      testName: 'Should return a correct string when nothing to be removed',
    },
    {
      input:
        ' Test    StringString&nbsp;String&nbsp; Ssds &nbsp;\n\n\n&nbsp;    &nbsp;        ',
      output: 'Test StringStringString Ssds',
      testName: 'Should work for a string with all the garbage',
    },
  ];

  testData.forEach(({ input, output, testName }) => {
    test(`${testName}`, async () => {
      const toNormalize = input;

      expect(normalizeText(toNormalize)).toEqual(output);
    });
  });

  test('Should throw an error when empty string passed', async () => {
    const toNormalize = '';

    expect(() => normalizeText(toNormalize)).toThrow(Error);
    expect(() => normalizeText(toNormalize)).toThrowError(
      'Empty string was passed!',
    );
  });
});
