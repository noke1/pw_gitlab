//https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project

export type MemberRequest = {
  id: number
  user_id: number
  access_level: AccessLevel
};

export type AccessLevel =
  | 0 //'NO ACCESS'
  | 5 //'MINIMAL ACCESS'
  | 10 //GUEST
  | 20 //REPORTER
  | 30 //DEVELOPER
  | 40 //MAINTAINER
  | 50; //OWNER
