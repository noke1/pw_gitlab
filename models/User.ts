export type User = {
  username: string
  password: string
  name: string
  email: string
  admin?: boolean
};

export type UserLogin = Pick<UserResponse, 'username'> & { password: string };

export type UserDashboardData = Pick<
UserResponse,
'name' | 'email' | 'created_at' | 'last_activity_on'
> & { projectsCount: number; groupsCount: number };

export type UserResponse = {
  id: number
  username: string
  name: string
  state: string
  avatar_url: string
  web_url: string
  created_at: string
  bio: string
  location: string
  public_email: string
  skype: string
  linkedin: string
  twitter: string
  discord: string
  website_url: string
  organization: string
  job_title: string
  pronouns: string
  bot: false
  work_information: string
  followers: number
  following: number
  is_followed: false
  local_time: string
  last_sign_in_at: string
  confirmed_at: string
  last_activity_on: string
  email: string
  theme_id: number
  color_scheme_id: number
  projects_limit: number
  current_sign_in_at: string
  identities: string[]
  can_create_group: boolean
  can_create_project: boolean
  two_factor_enabled: boolean
  external: boolean
  private_profile: boolean
  commit_email: string
  is_admin: boolean
  note: string
  namespace_id: number
  created_by: string
  highest_role: number
  current_sign_in_ip: string
  last_sign_in_ip: string
  sign_in_count: number
};
