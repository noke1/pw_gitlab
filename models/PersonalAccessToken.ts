export type PersonalAccessTokenRequest = {
  user_id: number
  name: string
  expires_at?: Date
  scopes: string[]
};

export type PersonalAccessTokenResponse = {
  id: string
  name: string
  revoked: boolean
  created_at: string
  scopes: string[]
  user_id: number
  last_used_at: string[]
  active: boolean
  expires_at: string
  token: string
};
