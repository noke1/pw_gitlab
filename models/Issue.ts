export type IssueRequest = {
  id: number
  title: string
};

export type IssueResponse = {
  id: number
  iid: number
  project_id: number
  title: string
  description: string
  state: string
  issue_type: IssueTypes
  assignee_id: number
  label: string
  milestone_id: number
  due_date: string
};

export type IssueMinimumDataUiModel = Pick<IssueResponse, 'issue_type' | 'title'>;

export type IssueMaximumDataUiModel = Pick<
IssueResponse,
| 'title'
| 'issue_type'
| 'description'
| 'assignee_id'
| 'milestone_id'
| 'label'
| 'due_date'
>;

export type IssueTypes = 'Issue' | 'Incident';
