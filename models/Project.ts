export type ProjectRequest = {
  name: string
  visibility?: ProjectVisibility
  description?: string
};

export type ProjectResponse = {
  id: number
  description: string
  name: string
  name_with_namespace: string
  path: string
  path_with_namespace: string
};

export type ProjectDashboardData = Pick<
ProjectResponse,
'name' | 'name_with_namespace'
>;

export type ProjectVisibility = 'public' | 'private' | 'internal';
